import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import Section from './components/Section';

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Section />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
});

export default App;
