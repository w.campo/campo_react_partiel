import React, { useState } from 'react';
import { View, ScrollView } from 'react-native';
import JeuxVideoList from './JeuxVideoList';
import AddJeuxVideo from './AddJeuxVideo';
import FilterJeuxVideo from './FilterJeuxVideo';
import Header from './Header';
import { styles } from '../styles';

const initialCategories = ["FPS", "Combat", "Sport", "Action-aventure", "Voiture", "Survival horror"];

const Section = () => {
  const [pseudo, setPseudo] = useState("Moi");
  const [jeux, setJeux] = useState([
    {
      name: "Medal of Honor",
      price: "10€",
      categorie: "FPS",
      vignette: "https://th.bing.com/th/id/OIP.hzRurpWiOsZOw40gx5dlJgHaHa?w=185&h=185&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 23124
    },
    {
      name: "Street Fighter 2",
      price: "15€",
      categorie: "Combat",
      vignette: "https://upload.wikimedia.org/wikipedia/commons/7/71/Street_Fighter_old_logo.png",
      id: 12349
    },
    {
      name: "Call of Duty",
      price: "20€",
      categorie: "FPS",
      vignette: "https://th.bing.com/th/id/OIP.trMaolRZhl9S-0OVgwQ9zwHaE8?w=273&h=182&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549706
    },
    {
      name: "NBA2K",
      price: "35€",
      categorie: "Sport",
      vignette: "https://th.bing.com/th/id/OIP.llS9g7DxdSQ1jrGKQxwEwgHaHa?w=161&h=180&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549763
    },
    {
      name: "God of War 2018",
      price: "25€",
      categorie: "Action-aventure",
      vignette: "https://th.bing.com/th/id/OIP.UdqvbDc6aCqCJB4I-IHytAHaEK?w=268&h=180&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549764
    },
    {
      name: "The Legend of Zelda : The Wind Waker",
      price: "35€",
      categorie: "Action-aventure",
      vignette: "https://th.bing.com/th/id/OIP.TOrHPbrtcP8ZpPOIOqceEwHaKr?w=126&h=181&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549765
    },
    {
      name: "Horizon : Forbidden West",
      price: "40€",
      categorie: "Action-aventure",
      vignette: "https://th.bing.com/th/id/OIP._wZaX33gQxPDq9-3E0MJFgHaEK?w=293&h=180&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549766
    },
    {
      name: "Forza Horizon 5",
      price: "50€",
      categorie: "Voiture",
      vignette: "https://th.bing.com/th/id/OIP.cs9XJ3j83smjo7B4L91C-QHaC8?w=288&h=138&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549767
    },
    {
      name: "The Last of Us",
      price: "55€",
      categorie: "Survival horror",
      vignette: "https://th.bing.com/th/id/OIP.fD4iwL3x8y_AHr5Lvb6m0AHaEK?rs=1&pid=ImgDetMain",
      id: 549768
    },
    {
      name: "Red Dead Redemption II",
      price: "18€",
      categorie: "Action-aventure",
      vignette: "https://th.bing.com/th/id/OIP.QKEUB1AjCRXB7_nV1DLYEgHaEK?w=323&h=181&c=7&r=0&o=5&dpr=1.4&pid=1.7",
      id: 549769
    },
  ]);

  const [categories, setCategories] = useState(initialCategories);
  const [categorie, setCategorie] = useState("");
  const [sortOrder, setSortOrder] = useState("");

  const handleAddJeu = (newJeu) => {
    setJeux([...jeux, newJeu]);
  };

  const handleDeleteJeu = (id) => {
    setJeux(jeux.filter(jeu => jeu.id !== id));
  };

  const handleAddCategorie = (newCategorie) => {
    setCategories([...categories, newCategorie]);
  };

  const filteredAndSortedJeux = () => {
    let filteredJeux = categorie
      ? jeux.filter(jeu => jeu.categorie === categorie)
      : jeux;

    if (sortOrder) {
      filteredJeux = filteredJeux.sort((a, b) => {
        const priceA = parseFloat(a.price.replace('€', ''));
        const priceB = parseFloat(b.price.replace('€', ''));
        return sortOrder === 'asc' ? priceA - priceB : priceB - priceA;
      });
    }

    return filteredJeux;
  };

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>
        <Header pseudo={pseudo} totalJeux={jeux.length} />
        <FilterJeuxVideo
          categorie={categorie}
          setCategorie={setCategorie}
          categories={categories}
          sortOrder={sortOrder}
          setSortOrder={setSortOrder}
        />
        <JeuxVideoList jeux={filteredAndSortedJeux()} onDelete={handleDeleteJeu} />
        <AddJeuxVideo onAddJeu={handleAddJeu} categories={categories} onAddCategorie={handleAddCategorie} />
      </View>
    </ScrollView>
  );
};

export default Section;
