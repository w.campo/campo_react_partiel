import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from '../styles';

const JeuxVideoItem = ({ jeu, onDelete }) => {
  return (
    <View style={styles.item}>
      <Image source={{ uri: jeu.vignette }} style={styles.image} />
      <Text style={styles.itemText}>{jeu.name}</Text>
      <Text style={styles.itemPrice}>{jeu.price}</Text>
      <Text style={styles.itemCategory}>#{jeu.categorie}</Text>
      <TouchableOpacity style={styles.button} onPress={() => onDelete(jeu.id)}>
        <Text style={styles.buttonText}>Supprimer</Text>
      </TouchableOpacity>
    </View>
  );
};

export default JeuxVideoItem;
