import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { styles } from '../styles';

const FilterJeuxVideo = ({ categorie, setCategorie, categories, sortOrder, setSortOrder }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>Filtrer par catégorie</Text>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          key="all"
          style={[
            styles.button,
            categorie === '' && styles.selectedButton,
          ]}
          onPress={() => setCategorie('')}
        >
          <Text
            style={[
              styles.buttonText,
              categorie === '' && styles.selectedButtonText,
            ]}
          >
            Toutes
          </Text>
        </TouchableOpacity>
        {categories.map(cat => (
          <TouchableOpacity
            key={cat}
            style={[
              styles.button,
              categorie === cat && styles.selectedButton,
            ]}
            onPress={() => setCategorie(cat)}
          >
            <Text
              style={[
                styles.buttonText,
                categorie === cat && styles.selectedButtonText,
              ]}
            >
              {cat}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <Text style={styles.label}>Trier par prix</Text>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={[
            styles.button,
            sortOrder === 'asc' && styles.selectedButton,
          ]}
          onPress={() => setSortOrder('asc')}
        >
          <Text
            style={[
              styles.buttonText,
              sortOrder === 'asc' && styles.selectedButtonText,
            ]}
          >
            Croissant
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.button,
            sortOrder === 'desc' && styles.selectedButton,
          ]}
          onPress={() => setSortOrder('desc')}
        >
          <Text
            style={[
              styles.buttonText,
              sortOrder === 'desc' && styles.selectedButtonText,
            ]}
          >
            Décroissant
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FilterJeuxVideo;
