import React from 'react';
import { View, Text } from 'react-native';
import { styles } from '../styles';

const Header = ({ pseudo, totalJeux }) => {
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>{pseudo}</Text>
      <Text style={styles.headerText}>Jeux: {totalJeux}</Text>
    </View>
  );
};

export default Header;
