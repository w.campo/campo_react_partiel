import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, Button, Image } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { styles } from '../styles';

const AddJeuxVideo = ({ onAddJeu, categories, onAddCategorie }) => {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [categorie, setCategorie] = useState('');
  const [newCategorie, setNewCategorie] = useState('');
  const [vignette, setVignette] = useState(null);

  const handleAddJeu = () => {
    if (name && price && (categorie || newCategorie)) {
      const finalCategorie = newCategorie || categorie;
      onAddJeu({
        name,
        price,
        categorie: finalCategorie,
        vignette: vignette ? vignette.uri : `https://loremflickr.com/80/80/${encodeURIComponent(name)}`,
        id: Date.now(),
      });


      if (newCategorie && !categories.includes(newCategorie)) {
        onAddCategorie(newCategorie);
      }

      setName('');
      setPrice('');
      setCategorie('');
      setNewCategorie('');
      setVignette(null);
    } else {
      alert('Veuillez remplir tous les champs');
    }
  };

  const handleChooseImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.canceled) {
      setVignette(result.assets[0]);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Titre</Text>
      <TextInput
        style={styles.input}
        value={name}
        onChangeText={setName}
        placeholder="Titre du jeu"
      />
      <Text style={styles.label}>Tarif</Text>
      <TextInput
        style={styles.input}
        value={price}
        onChangeText={setPrice}
        placeholder="Tarif"
        keyboardType="numeric"
      />
      <Text style={styles.label}>Catégorie</Text>
      <View style={styles.buttonsContainer}>
        {categories.map(cat => (
          <TouchableOpacity
            key={cat}
            style={[
              styles.button,
              categorie === cat && styles.selectedButton,
            ]}
            onPress={() => setCategorie(cat)}
          >
            <Text
              style={[
                styles.buttonText,
                categorie === cat && styles.selectedButtonText,
              ]}
            >
              {cat}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <Text style={styles.label}>Ou ajouter une nouvelle catégorie</Text>
      <TextInput
        style={styles.input}
        value={newCategorie}
        onChangeText={setNewCategorie}
        placeholder="Nouvelle catégorie"
      />
      <Button title="Choisir une image" onPress={handleChooseImage} />
      {vignette && (
        <Image
          source={{ uri: vignette.uri }}
          style={styles.image}
        />
      )}
      <TouchableOpacity style={styles.button} onPress={handleAddJeu}>
        <Text style={styles.buttonText}>Ajouter +</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddJeuxVideo;
