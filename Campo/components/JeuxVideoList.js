import React from 'react';
import { View, ScrollView } from 'react-native';
import JeuxVideoItem from './JeuxVideoItem';
import { styles } from '../styles';

const JeuxVideoList = ({ jeux, onDelete }) => {
  return (
    <ScrollView contentContainerStyle={styles.list}>
      <View style={styles.listContainer}>
        {jeux.map(jeu => (
          <JeuxVideoItem key={jeu.id} jeu={jeu} onDelete={onDelete} />
        ))}
      </View>
    </ScrollView>
  );
};

export default JeuxVideoList;
